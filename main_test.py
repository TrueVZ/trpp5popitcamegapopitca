from main import convertator_weather


def test():
    assert float(convertator_weather("Fahrenheit", 32, "Celsius")) == 0
    assert float(convertator_weather("Kelvin", 273, "Celsius")) == 0
    assert float(convertator_weather("Celsius", 32, "Kelvin")) == 305
    assert float(convertator_weather("Celsius", 45, "Fahrenheit")) == 113
    assert float(convertator_weather("Fahrenheit", 23, "Kelvin")) == 268
    assert float(convertator_weather("Kelvin", 308, "Fahrenheit")) == 95


