import sys

def convertator_weather(input, value, output):
    if input == "Celsius":
        if output == "Fahrenheit":
            return (value * 9 / 5 + 32)
        elif (output == "Kelvin"):
            return (value + 273)

    elif input == "Fahrenheit":
        if output == "Celsius":
            return (value - 32) * 5 / 9
        elif output == "Kelvin":
            return (value - 32) * 5 / 9 + 273

    elif input == "Kelvin":
        if output == "Celsius":
            return (value - 273)
        elif output == "Fahrenheit":
            return ((value - 273) * 9 / 5 + 32)


if __name__ == '__main__':
    input = sys.argv[1]  # системные аргументы, передающиеся в качестве параметров
    value = int(sys.argv[2])
    output = sys.argv[3]
    print(convertator_weather(input, value, output))
